import re
import time
import csv
import json
import requests
from fake_useragent import UserAgent


class KeywordGetter:
    def __init__(self):
        self.queue = set()
        self.country = "us"
        self.language = "en"
        self.threshold_count = 200
        self.results = []

    def checkSeedKeywordExists(self, keyword, meta_keyword):
        keyword_ = re.sub('[^A-Za-z0-9]+', '', keyword)
        if seed_keyword in keyword or seed_keyword in keyword_:
            return True
        else:
            return False

    def fetchSuggestion(self, keyword, seed_keyword, meta_keyword):
        """ return list of suggestion based on the geolocation and language for a keyword """
        url = "http://suggestqueries.google.com/complete/search?client=chrome&hl={}&gl={}&callback=?&q={}".format(
            self.language, self.country, keyword)
        ua = UserAgent(use_cache_server=False, verify_ssl=False)
        headers = {"user-agent": ua.chrome, "dataType": "jsonp"}

        response = requests.get(url, headers=headers, verify=True)
        suggestions = json.loads(response.text)

        sugg = []
        index = 0
        relevancies = []
        if "google:suggestrelevance" in suggestions[4].keys():
            relevancies = suggestions[4]['google:suggestrelevance']
        for word in suggestions[1]:
            if self.checkSeedKeywordExists(word, meta_keyword):
                sugg.append({
                    'keyword': word,
                    'relevancy_score': relevancies[index] if len(relevancies) > 0 else None,
                    'seed_keyword': seed_keyword,
                    'meta_keyword': meta_keyword,
                })
            else:
                continue
            index += 1

        return sugg

    def fetchRelatedkeywords(self, keyword, meta_keyword):
        """ return all questions for a keyword with search volumes """
        suffix = ["", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l",
                  "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w" "x", "y", "z"]
        suffix_arr = list(map(lambda x: keyword+" "+x, suffix))
        duplicates = set()
        for word in suffix_arr:
            suggestion = self.fetchSuggestion(word, keyword, meta_keyword)
            for query in suggestion:
                if query['keyword'] not in duplicates:
                    duplicates.add(query['keyword'])
                    self.results.append(query)
                    self.queue.add(query['keyword'])


if __name__ == "__main__":

    flag = True
    # for single keyword
    meta_keyword = "airfryer"
    # writes  keywords data into file
    ofile = open("{}_keywords.csv".format(meta_keyword), 'w+')
    writer = csv.DictWriter(ofile, fieldnames=[
                            "keyword", "relevancy_score", "seed_keyword", "meta_keyword"])
    writer.writeheader()

    keyword_getter = KeywordGetter()
    keyword_getter.queue.add(meta_keyword)
    while(flag):
        ofile = open("{}_keywords.csv".format(meta_keyword), 'a')
        writer = csv.DictWriter(ofile, fieldnames=[
                                "keyword", "relevancy_score", "seed_keyword", "meta_keyword"])
        try:
            seed_keyword = keyword_getter.queue.pop()
            keyword_getter.fetchRelatedkeywords(seed_keyword, meta_keyword)
            
            writer.writerows(keyword_getter.results)
            print(keyword_getter.results)
            ofile.close()
            if len(keyword_getter.results) > keyword_getter.threshold_count:
                flag = False
        except:
            time.sleep(10)
        keyword_getter.results = []