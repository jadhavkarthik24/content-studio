import networkx as nx
import pandas as pd
import matplotlib.pyplot as plt
from pyvis.network import Network

nodes = []
# # net = Network(height='750px', width='100%', bgcolor='#222222',
# #               font_color='white', directed=True)
net = Network(height='750px', width='100%',
              bgcolor='#222222', font_color='white')

# # # set the physics layout of the network
# # net.barnes_hut()
df = pd.read_csv('/home/jkd/Downloads/keywords.csv')

sources = df['keyword']
targets = df['seed_keyword']

sources_unique = list(df.keyword.unique())
nodes.extend(sources_unique)
target_unique = list(df.seed_keyword.unique())
nodes.extend(target_unique)

for node in set(nodes):
    net.add_node(node, title=node)

edge_data = zip(sources, targets)

for e in edge_data:
    src = e[0]
    dst = e[1]

    net.add_edge(dst, src)

neighbor_map = net.get_adj_list()

# add neighbor data to node hover data
for node in net.nodes:
    node['title'] += ' Neighbors::<br>' + '<br>'.join(neighbor_map[node['id']])
    node['value'] = len(neighbor_map[node['id']])

# net.enable_physics(True)
net.show('network_graph.html')
# net.show_buttons(filter_=['physics'])


# G = nx.Graph()

# G.add_nodes_from(nodes)

# edges = []
# for e in edge_data:
#     src = e[0]
#     dst = e[1]
#     edges.append((src, dst))
# G.add_edges_from(edges)

# nx.draw(G)
# # plt.show()
# plt.savefig(“graph.png”) 

