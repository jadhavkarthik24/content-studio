import time
from django.db.models import Count
from .models import Keywords, CronJobs, SeedKeywords, fetchRelatedKeyword, fetchSuggestion
from .serializers import KeywordsSerializer, CronsSerializer, SeedKeywordSerializer
from .searchvolume import searchVolumePuller
from django.core.exceptions import ObjectDoesNotExist


def fetchkeywords():
    crons = CronJobs.objects.filter(cron_name="Fetch Keywords")
    # runs fetch keywords cron sequentially
    if len(crons) == 0:
        serializer = CronsSerializer(data={
            "cron_status": False,
            "cron_name": "Fetch Keywords"
        })
        if serializer.is_valid():
            serializer.save()
        return
    if crons[0].cron_status == False:
        crons[0].cron_status = True
        crons[0].save()
        seed_keywords = SeedKeywords.objects.filter(
            already_fetched=False).order_by('-search_volume')[:3]
        for keyword in seed_keywords:
            keywords = fetchRelatedKeyword(keyword.keyword, keyword.keyword)
            serializer = KeywordsSerializer(data=keywords, many=True)
            if serializer.is_valid():
                serializer.save()
                # changing status to fetched data
                # Keywords.objects.filter(keyword=keyword.keyword).update(already_fetched=True)
                keyword.already_fetched = True
                keyword.save()
            # else:
            #     print(serializer.errors)
        time.sleep(5)
        crons[0].cron_status = False
        crons[0].save()
    else:
        return


def fetchKeywordsRecursively():
    crons = CronJobs.objects.filter(cron_name="Fetch Keywords Recursively")
    # runs fetch keywords cron sequentially
    if len(crons) == 0:
        serializer = CronsSerializer(data={
            "cron_status": False,
            "cron_name": "Fetch Keywords Recursively"
        })
        if serializer.is_valid():
            serializer.save()
        return
    if crons[0].cron_status == False:
        crons[0].cron_status = True
        crons[0].save()
        seed_keywords = Keywords.objects.filter(
            already_fetched=False).order_by('id')[:1]
        for keyword in seed_keywords:
            # keywords_ = SeedKeywords.objects.filter(keyword=keyword.keyword)
            # if len(keywords_) > 0:
            #     keyword.already_fetched = True
            #     keyword.save()
            #     continue
            try:
                keywords = fetchRelatedKeyword(
                    keyword.keyword, keyword.meta_keyword)
            except:
                time.sleep(30)
                crons[0].cron_status = False
                crons[0].save()
                return
            for keyword_dict in keywords:
                serializer = KeywordsSerializer(data=keyword_dict)
                if serializer.is_valid():
                    serializer.save()
                # else:
                #     print(serializer.errors)
            # changing status to fetched data
            duplicates_keyword = Keywords.objects.filter(
                keyword=keyword.keyword)
            if len(duplicates_keyword) > 1:
                duplicates_keyword.update(already_fetched=True)
            else:
                keyword.already_fetched = True
                keyword.save()
            # appending  already fetched data to seed keywords table
            # seed_serializer = SeedKeywordSerializer(data={
            #     'keyword': keyword.keyword,
            #     'search_volume': keyword.search_volume,
            #     'cpc': keyword.cpc,
            #     "competition": keyword.competition,
            #     "already_fetched": True
            # })
            # if seed_serializer.is_valid():
            #     seed_serializer.save()
        time.sleep(1)
        crons[0].cron_status = False
        crons[0].save()
        return
    else:
        return


def googleAdsSearchVolume():
    CLIENT_ID = "971473896907-jc7si1speeh6c2pu71o3fnnjrfn2jm7g.apps.googleusercontent.com"
    CLIENT_SECRET = "vnUTc7TrgjqBz-kJ0UFdtmNk"
    REFRESH_TOKEN = "1//0g5xwUzGlt7NVCgYIARAAGBASNwF-L9Ir7tgjNonkYcfujz8tAwE7iZr6Dh8Ybnslv3EkfEsZ52EtvW2LaAMC6bas7ePD3ZAn-Kk"
    DEVELOPER_TOKEN = "ZhrDO0V9zDCoLQbvjpCvsg"
    CLIENT_CUSTOMER_ID = "9651475560"
    # fetches keyword array
    keyword_list = Keywords.objects.filter(search_volume__isnull=True).values_list(
        'keyword', flat=True).order_by('id')[:700]
    if len(keyword_list) <= 0:
        return
    keyword_list = list(set(keyword_list))
    volume_puller = searchVolumePuller(CLIENT_ID,
                                       CLIENT_SECRET,
                                       REFRESH_TOKEN,
                                       DEVELOPER_TOKEN,
                                       CLIENT_CUSTOMER_ID)
    adwords_client = volume_puller.get_client()

    targeting_service = volume_puller.get_service(
        'TargetingIdeaService', adwords_client)

    search_volume = volume_puller.get_search_volume(
        targeting_service, keyword_list)

    for volume in search_volume:
        try:
            obj = Keywords.objects.get(keyword=volume['query'])
            obj.cpc = volume['cpc']
            obj.competition = volume['competition']
            obj.search_volume = volume['search_volume']
            obj.save()
        except ObjectDoesNotExist:
            continue
    return True


def removeDuplicates():
    keywords = Keywords.objects.values('keyword').annotate(
        keyword_count=Count('keyword')).filter(keyword_count__gt=1)
    if len(keywords) > 0:
        for keyword in keywords:
            keyword = keyword['keyword']
            obj = Keywords.objects.filter(
                keyword=keyword).values_list('pk', flat=True)[1:]
            ids = list(obj)
            query = Keywords.objects.filter(pk__in=ids)
            query.delete()
    return
