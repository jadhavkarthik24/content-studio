import csv
from rest_framework.views import APIView
from .serializers import SeedKeywordSerializer, SeedKeywordsStatSerializer, KeywordStatStatSerializer
from .models import Keywords, SeedKeywords
from rest_framework.response import Response
from rest_framework import status
from io import StringIO
from rest_framework.generics import ListAPIView
from django.shortcuts import render


# Create your views here.

required_fields = ("Keyword", "Search Volume", "CPC")


class UploadKeywordsData(APIView):

    def post(self, request, format=None):
        # to access files
        if 'csv_file' not in request.FILES:
            return Response({"message": "Please upload a csv file"}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        data = StringIO(request.FILES['csv_file'].read().decode('utf-8-sig'))

        reader = csv.DictReader(data)
        fieldnames = reader.fieldnames
        reader.fieldnames = "keyword", "search_volume", "cpc"
        missing_fields = set(required_fields) - set(fieldnames)
        if len(missing_fields) > 0:
            msg = "{} headers are missing".format(",".join(list(missing_fields))) if len(
                missing_fields) > 1 else "{} header is missing".format(",".join(list(missing_fields)))
            return Response({"message": msg}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        for row in reader:
            serializer = SeedKeywordSerializer(data=row)
            if serializer.is_valid():
                serializer.save()
            else:
                return Response({"message": list(serializer.errors.keys())[0]+' - '+list(serializer.errors.values())[0][0]})
        return Response({'message': "Successfully uploaded"})


class SeedStatListView(ListAPIView):
    queryset = SeedKeywords.objects.raw('SELECT seed.id,seed.keyword,seed.search_volume,SUM(kw.search_volume) AS total_volume,COUNT(DISTINCT kw.keyword) AS unique_keywords,SUM(CASE WHEN (kw.keyword LIKE "what %%" OR kw.keyword LIKE "why %%" OR kw.keyword LIKE "where %%" OR kw.keyword LIKE "who %%" OR kw.keyword LIKE "how %%" OR kw.keyword LIKE "do %%" OR kw.keyword LIKE "does %%" OR kw.keyword LIKE "will %%" OR kw.keyword LIKE "can %%" OR kw.keyword LIKE "are %%") THEN 1 ELSE 0 END) AS total_questions FROM `contentstudio_seedkeywords` seed INNER JOIN contentstudio_keywords kw ON kw.meta_keyword=seed.keyword  GROUP BY kw.meta_keyword')
    serializer_class = SeedKeywordsStatSerializer

    def list(self, request):
        queryset = self.get_queryset()
        # the serializer didn't take my RawQuerySet, so made it into a list
        serializer = SeedKeywordsStatSerializer(list(queryset), many=True)
        return Response(serializer.data)


class KeywordStatListView(ListAPIView):
    queryset = []
    serializer_class = KeywordStatStatSerializer

    def list(self, request):
        queryset = self.get_queryset()
        page = request.query_params.get('page', 0)
        seed_keyword = request.query_params.get('seed_keyword', None)
        if seed_keyword:
            queryset = Keywords.objects.raw('SELECT id,keyword,search_volume,cpc,competition,seed_keyword,(CASE WHEN (keyword LIKE "what %%" OR keyword LIKE "why %%" OR keyword LIKE "where %%" OR keyword LIKE "who %%" OR keyword LIKE "how %%" OR keyword LIKE "do %%" OR keyword LIKE "does %%" OR keyword LIKE "will %%" OR keyword LIKE "can %%" OR keyword LIKE "are %%") THEN 1 ELSE 0 END) AS is_question FROM `contentstudio_keywords` {} ORDER BY `id` ASC LIMIT {} ,50'.format("WHERE meta_keyword='"+seed_keyword+"'", page))

        # the serializer didn't take my RawQuerySet, so made it into a list
        serializer = KeywordStatStatSerializer(list(queryset), many=True)
        return Response(serializer.data)


def seedkeywordstats(request):
    queryset = SeedKeywords.objects.raw('SELECT seed.id,seed.keyword,seed.search_volume,SUM(kw.search_volume) AS total_volume,COUNT(DISTINCT kw.keyword) AS unique_keywords,SUM(CASE WHEN (kw.keyword LIKE "what %%" OR kw.keyword LIKE "why %%" OR kw.keyword LIKE "where %%" OR kw.keyword LIKE "who %%" OR kw.keyword LIKE "how %%" OR kw.keyword LIKE "do %%" OR kw.keyword LIKE "does %%" OR kw.keyword LIKE "will %%" OR kw.keyword LIKE "can %%" OR kw.keyword LIKE "are %%") THEN 1 ELSE 0 END) AS total_questions FROM `contentstudio_seedkeywords` seed INNER JOIN contentstudio_keywords kw ON kw.meta_keyword=seed.keyword  GROUP BY kw.meta_keyword')
    serializer_class = SeedKeywordsStatSerializer
    serializer = SeedKeywordsStatSerializer(list(queryset), many=True)
    context = {
        "keywords": serializer.data
    }
    return render(request, 'index.html', context=context)


def keywordstats(request):
    queryset = []
    serializer_class = KeywordStatStatSerializer
    page = 0
    if 'page' in request.GET:
        page = request.GET['page']

    if "seed_keyword" in request.GET and request.GET['seed_keyword'] != "":
        seed_keyword = request.GET['seed_keyword']
        queryset = Keywords.objects.raw('SELECT DISTINCT keyword,id,search_volume,cpc,competition,seed_keyword,(CASE WHEN (keyword LIKE "what %%" OR keyword LIKE "why %%" OR keyword LIKE "where %%" OR keyword LIKE "who %%" OR keyword LIKE "how %%" OR keyword LIKE "do %%" OR keyword LIKE "does %%" OR keyword LIKE "will %%" OR keyword LIKE "can %%" OR keyword LIKE "are %%") THEN 1 ELSE 0 END) AS is_question FROM `contentstudio_keywords` {} GROUP BY keyword ORDER BY `search_volume` DESC'.format("WHERE meta_keyword='"+seed_keyword+"'"))

    serializer = KeywordStatStatSerializer(list(queryset), many=True)
    context = {
        "keywords": serializer.data
    }
    return render(request, 'keywords_stat.html', context=context)
