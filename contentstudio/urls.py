from django.urls import path
from .views import UploadKeywordsData, SeedStatListView, KeywordStatListView, seedkeywordstats, keywordstats

urlpatterns = [
    path("upload/", UploadKeywordsData.as_view(), name="keyword_upload"),
    # path("seed-keyword/stats/", SeedStatListView.as_view(),
    #      name="seed_keyword_stats"),
    # path("keyword/stats/", KeywordStatListView.as_view(), name="keyword_stats"),
    path("seed-keyword/stats/", seedkeywordstats, name="seedkeywordstats"),
    path("keyword/stats/", keywordstats, name="keywordstats"),

]
