from django.apps import AppConfig


class ContentstudioConfig(AppConfig):
    name = 'contentstudio'
