from rest_framework import serializers
from .models import Keywords, CronJobs, SeedKeywords


class KeywordsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Keywords
        fields = ('keyword', 'seed_keyword',
                  'relevancy_score', 'meta_keyword',)


# class QuestionsSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Questions
#         fields = ('question', 'seed_keyword',
#                   'relevancy_score')


class SeedKeywordSerializer(serializers.ModelSerializer):
    class Meta:
        model = SeedKeywords
        fields = ('keyword', 'search_volume', 'cpc',
                  "competition", "already_fetched")


class CronsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CronJobs
        fields = ('cron_status', 'cron_name',)


class SeedKeywordsStatSerializer(serializers.Serializer):
    keyword = serializers.CharField(max_length=255, required=True)
    search_volume = serializers.IntegerField(required=True)
    unique_keywords = serializers.IntegerField(required=True)
    total_questions = serializers.IntegerField(required=True)
    total_volume = serializers.IntegerField(required=True)

    class Meta:
        fields = ('keyword', 'search_volume','unique_keywords','total_questions','total_volume')

class KeywordStatStatSerializer(serializers.Serializer):
    keyword = serializers.CharField(max_length=255, required=True)
    search_volume = serializers.IntegerField(required=True)
    cpc = serializers.FloatField()
    competition = serializers.FloatField()
    seed_keyword = serializers.CharField(max_length=255, required=True)
    is_question = serializers.BooleanField()

    class Meta:
        fields = ('keyword', 'search_volume','cpc','competition','seed_keyword','is_question')