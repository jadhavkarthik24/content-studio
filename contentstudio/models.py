import re
import json
import requests
from django.db import models
from fake_useragent import UserAgent


class SeedKeywords(models.Model):
    keyword = models.CharField(max_length=255)
    search_volume = models.IntegerField(null=True)
    cpc = models.FloatField(null=True, blank=True)
    industry = models.CharField(max_length=100, null=True)
    competition = models.FloatField(null=True, blank=True)
    # status checks whether related keywords fetched or not for a seed keyword
    already_fetched = models.BooleanField(default=False)
    added_dt = models.DateField(auto_now_add=True, blank=True, null=True)


class Keywords(models.Model):
    # seed keyword
    keyword = models.TextField(max_length=512)
    search_volume = models.IntegerField(null=True)
    seed_keyword = models.CharField(max_length=255, null=True)
    # parent keywords through which all the keywords are fetched
    meta_keyword = models.CharField(max_length=255, null=True)
    relevancy_score = models.IntegerField(null=True)
    cpc = models.FloatField(null=True, blank=True)
    competition = models.FloatField(null=True, blank=True)
    # status checks whether keywords fetched or not for a query
    already_fetched = models.BooleanField(default=False)
    added_dt = models.DateField(auto_now_add=True, blank=True, null=True)

    def __str__(self):
        return self.keyword

    class Meta:
        ordering = ('-search_volume',)
        indexes = [
            models.Index(fields=['keyword', ]),
        ]

class CronJobs(models.Model):
    cron_name = models.CharField(max_length=200, null=True)
    cron_status = models.BooleanField(default=False)
    rate_limit = models.IntegerField(default=0)


def checkSeedKeywordExists(keyword, seed_keyword):
    keyword_ = re.sub('[^A-Za-z0-9]+', '', keyword)
    if seed_keyword in keyword or seed_keyword in keyword_:
        return True
    else:
        return False


def fetchSuggestion(query, seed_keyword, meta_keyword, country="us", language="en"):
    """ return list of suggestion based on the geolocation and language for a keyword """
    url = "http://suggestqueries.google.com/complete/search?client=chrome&hl={}&gl={}&callback=?&q={}".format(
        language, country, query)
    ua = UserAgent(use_cache_server=False, verify_ssl=False)
    headers = {"user-agent": ua.chrome, "dataType": "jsonp"}

    response = requests.get(url, headers=headers, verify=True)
    suggestions = json.loads(response.text)
    # rate limit increment
    # obj = CronJobs.objects.get(pk=1)
    # rate_limit = obj.rate_limit
    # obj.rate_limit = rate_limit+1
    # obj.save()

    sugg = []
    index = 0
    relevancies = []
    if "google:suggestrelevance" in suggestions[4].keys():
        relevancies = suggestions[4]['google:suggestrelevance']
    for word in suggestions[1]:
        if checkSeedKeywordExists(word, meta_keyword):
            sugg.append({
                'keyword': word,
                'relevancy_score': relevancies[index] if len(relevancies) > 0 else None,
                'seed_keyword': seed_keyword,
                'meta_keyword': meta_keyword,
            })
        else:
            continue
        index += 1

    return sugg


def fetchSearchVolumes(keywords):
    """ returns search volume for the list of keywords """
    url = 'https://db2.keywordsur.fr/keyword_surfer_keywords?country=us&keywords={}'.format(
        keywords)
    ua = UserAgent()
    headers = {"user-agent": ua.chrome}
    response = requests.get(url, headers=headers, verify=True)
    volume = json.loads(response.text)

    suggestions_vol = {}
    for key, value in volume.items():
        suggestions_vol[key] = {
            'search_volume': value['search_volume'],
            'cpc': value['cpc'],
            'competition': value['competition']
        }
        for keyword in value['similar_keywords']:
            keyword.update({"seed_keywords": key})
    return suggestions_vol


def fetchkeywordquestions(keyword, country="us", language="en"):
    """ return all questions for a keyword with search volumes """

    prefix = ["why",  "what", "where", "when", "who",
              "are", "can", "does", "do", "will"]
    prefix_arr = list(map(lambda x: x+" "+keyword, prefix))
    questions = []
    duplicates = set()
    for word in prefix_arr:
        suggestion = fetchSuggestion(word, 0, country, language)
        for query in suggestion:
            if query['question'] not in duplicates:
                duplicates.add(query['question'])
                questions.append(query)

    return questions


def fetchRelatedKeyword(keyword, meta_keyword, country="us", language="en"):
    """ return all questions for a keyword with search volumes """
    suffix = ["", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l",
              "m", "n", "o", "p", "q", "r", "s", "t", "u", "v","w" "x", "y", "z"]
    suffix_arr = list(map(lambda x: keyword+" "+x, suffix))
    keywords = []
    duplicates = set()
    for word in suffix_arr:
        suggestion = fetchSuggestion(word, keyword, meta_keyword, country, language)
        for query in suggestion:
            if query['keyword'] not in duplicates:
                duplicates.add(query['keyword'])
                keywords.append(query)
    return keywords
