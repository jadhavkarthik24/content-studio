import _locale
import googleads
import pandas as pd
import traceback
from googleads import adwords
from googleads import oauth2

_locale._getdefaultlocale = (lambda *args: ['en_UK', 'UTF-8'])


class searchVolumePuller():
    def __init__(self, client_ID, client_secret, refresh_token, developer_token, client_customer_id):
        self.client_ID = client_ID
        self.client_secret = client_secret
        self.refresh_token = refresh_token
        self.developer_token = developer_token
        self.client_customer_id = client_customer_id

    def get_client(self):
        access_token = oauth2.GoogleRefreshTokenClient(self.client_ID,
                                                       self.client_secret,
                                                       self.refresh_token)
        adwords_client = adwords.AdWordsClient(self.developer_token,
                                               access_token,
                                               client_customer_id=self.client_customer_id,
                                               cache=googleads.common.ZeepServiceProxy.NO_CACHE)

        return adwords_client

    def get_service(self, service, client):

        return client.GetService(service)

    def get_search_volume(self, service_client, keyword_list):
        keywords_and_search_volume = []
        #need to split data into smaller lists of 700#
        sublists = [keyword_list[x:x+700]
                    for x in range(0, len(keyword_list), 700)]
        for sublist in sublists:
            # Construct selector and get keyword stats.
            offset = 0
            selector = {
                'searchParameters': [

                    {
                        'xsi_type': 'RelatedToQuerySearchParameter',
                        'queries': keyword_list
                    },
                    {
                        # Language setting (optional).
                        # The ID can be found in the documentation:
                        #  https://developers.google.com/adwords/api/docs/appendix/languagecodes
                        'xsi_type': 'LanguageSearchParameter',
                        'languages': [{'id': 1000}]
                    },
                    {
                        # Location setting (optional).
                        # The ID can be found in the documenation:
                        #  https://developers.google.com/adwords/api/docs/appendix/geotargeting
                        # Country codes found by searching country name in name filter
                        'xsi_type': 'LocationSearchParameter',
                        'locations': [{'id': 2840}]
                    },
                    {
                        # Network search parameter (optional)
                        'xsi_type': 'NetworkSearchParameter',
                        'networkSetting': {
                            'targetGoogleSearch': True,
                            'targetSearchNetwork': False,
                            'targetContentNetwork': False,
                            'targetPartnerSearchNetwork': False

                        }
                    }

                ],
                'ideaType': 'KEYWORD',
                'requestType': 'STATS',
                'requestedAttributeTypes': ['KEYWORD_TEXT', 'SEARCH_VOLUME', 'AVERAGE_CPC', 'COMPETITION', 'TARGETED_MONTHLY_SEARCHES'],
                'paging': {
                    'startIndex': str(offset),
                    'numberResults': str(len(sublist))
                }
            }
            #pull the data#
            page = service_client.get(selector)
            #access json elements to return the suggestions#
            for i in range(0, len(page['entries'])):
                keywords = (page['entries'][i]
                                ['data'][0]['value']['value'])
                competition = (page['entries'][i]['data'][1]['value']['value'])
                average_cpc = (
                    (page['entries'][i]['data'][3]['value']['value']['microAmount'])/1000000)
                search_volume = (page['entries'][i]
                                 ['data'][4]['value']['value'])
                keywords_and_search_volume.append({
                    "query": keywords,
                    "search_volume": search_volume,
                    "cpc": average_cpc,
                    "competition": competition
                })

        return keywords_and_search_volume


if __name__ == '__main__':
    CLIENT_ID = "559533277054-po7aj50pbhp4kanaq3lql1dib4i1vd0b.apps.googleusercontent.com"
    CLIENT_SECRET = "4D5130rb-hKOGFHsI2oTOvHm"
    REFRESH_TOKEN = "1//0gJlndj5EbE4zCgYIARAAGBASNwF-L9IrnU2Kul2U5tk4tlrzzKpO5_VQ6-7gWFli6uc_Y5pVz2ynJYujlIvgxfR5mSXEwE0amN4"
    # DEVELOPER_TOKEN = "fFt01ksTGafatyagd0zUKg"
    DEVELOPER_TOKEN = "XL_K8HDymUPmbuvDPipjdA"

    CLIENT_CUSTOMER_ID = "4731829043"

    keyword_list = ['antibacterial']
    volume_puller = searchVolumePuller(CLIENT_ID,
                                       CLIENT_SECRET,
                                       REFRESH_TOKEN,
                                       DEVELOPER_TOKEN,
                                       CLIENT_CUSTOMER_ID)
    adwords_client = volume_puller.get_client()

    targeting_service = volume_puller.get_service(
        'TargetingIdeaService', adwords_client)
    print(volume_puller.get_search_volume(targeting_service, keyword_list))
